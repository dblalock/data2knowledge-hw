# Course: STAT3080/APMA3501
# Instructor: Steiner
#
# Homework 1
# Authors: Davis Blalock, Dru Knox, Thomas Coles, Sung Kim
#
# Group Pledge: On our honor we have neither given nor received
# unauthorized aid on this assignment.

###############################################################
# Problem 1.4
cat('\n----------1.4----------\n')

# 1
print( 1+2*(3+4) )
# [1] 15

# 2
print( (4^3)+3^(2+1) )
# [1] 91

# 3
print( sqrt((4+3)*(2+1)) )
# [1] 4.582576

# 4
print( ((1+2)/(3+4))^2 )
# [1] 0.1836735

# answer
print("here's the line that spits out the answer")

##############################################################
# Problem 1.6
cat('\n----------1.6----------\n')

p<-c(2, 3, 4, 7, 11, 13, 17, 19)
print(p)
print(length(p))

###############################################################
# Problem 1.8
cat('\n----------1.18----------\n')

x<-c(2, 5, 4, 10, 8)
print(x)
print( x^2 )
print( x-6 )
print( (x-9)^2 )

###############################################################
# Problem 1.12
cat('\n----------1.12----------\n')

print( rep("a",5) )
print( seq(1,99,2) )
print( c(rep(1,3), rep(2,3), rep(3,3)) )
print( c(rep(1,3), rep(2,2), 3) )
print( c(1:5, 4:1))

###############################################################
# Problem 1.13
cat('\n----------1.13----------\n')

x1 = c(1,2,3,5,8,13,21,34); print(x1)
x2 = 1:10; 					print(x2)
x3 = 1/x2; 					print(x3)
x4 = (1:6)^3;				print(x4)
x5 = 1964:2003;				print(x5)
x6 = c(14,18,23,28,34,42,50,59,66,72,79,86,96,103,110); print(x6)
x7 = seq(0,1000,25);		print(x7)

###############################################################
# Problem 1.14 + Additional Problems
cat('\n----------1.14 + Additional Questions----------\n')

# Make the commutes vector and print relevant statistics
commutes = c(17, 16, 20, 24, 22, 15, 21, 15, 17, 22)
print( commutes )
print( max(commutes) )
print( mean(commutes) )
print( min(commutes) )

# Modify the commutes vector and reprint the mean
commutes[4] = 18
print( commutes )
print( mean(commutes) )

# Print the number of commutes >= 20 min
print( sum(commutes >= 20) )
# Print the percentage of commutes < 18
print( sum(commutes < 18)/length(commutes) )

# Sort the commutes array in various ways
commutes.sort = sort(commutes)
commutes.revsort = sort(commutes, decreasing = TRUE)
print( commutes.sort )
print( commutes.revsort )

# Print out ranges of elements, reverse the order of printing
print( commutes.sort[1:5] )
print( commutes.revsort[6:10] )
print( commutes.revsort[10:6] )

###############################################################
# Problem 1.23
cat('\n----------1.23----------\n')

print( length(treering) )
print( min(treering) )
print( max(treering) )
print( sum(treering > 1.5) )

###############################################################
# Problem 1.25
cat('\n----------1.25----------\n')
library("UsingR")
printf <- function(...) print(sprintf(...))
toInt <- function(x) return( as.integer(round(x)) )

# 1
times = nym.2002$time
print( length(times) )

# 2
minTime = min(times);
printf("%f minutes", minTime)
minTimeHours = toInt(minTime/60);
minTimeMinutes = toInt(minTime %% 60);
printf("%d hours and %d minutes (rounded)", minTimeHours, minTimeMinutes)

# 3
maxTime = max(times);
printf("%f minutes", maxTime)
maxTimeHours = toInt(maxTime / 60);
maxTimeMinutes = toInt(maxTime %% 60);
printf("%d hours and %d minutes (rounded)", maxTimeHours, maxTimeMinutes)

###############################################################
# Problem 9
cat('\n----------Problem 9----------\n')

numbers = list(2,5,9,13)
randoms = list(1:3,c("UVA","Cavaliers"), 3+2i)
activities = list(sports=c("soccer","rugby"), music=c("ipod","cd"))

# a
print( numbers[[3]] )

# b
print( randoms[[1]][3] )

# c
print( randoms[[2]] )

# d
activities[[1]][2]
activities$sports[2]

# e
print( activities[[1]] )
print( activities$sports )

###############################################################
# Problem 10
cat('\n----------Problem 10----------\n')

# a
for( i in 1:length(x) ) {
	print(x[i]^3)
}

# b
for( i in 1:3 ) {
	print( x[i] + x[length(x) - i + 1])
}

###############################################################
# Additional Problem 11
cat('\n----------Problem 11----------\n')

# A
A = matrix(nrow=2, ncol=3, byrow=TRUE, c(
				+ 2,7,1,
				+ 5,3,9
		))
B = matrix(nrow=3, ncol=2, c(
				+ 1,9,5,
				+ 3,8,7
		))
print(A)
print(B)

# B
print(A%*%B)	# matrix multiply
print(B%*%A)	# matrix multiply
print(3*A)		# scale by 3
print(dim(A))
print(t(B))
C = matrix(nrow=2,byrow=TRUE,1:4);
print( solve(C) )
